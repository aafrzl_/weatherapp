
![Logo](https://i.imgur.com/uhJornJ.png)


# Aplikasi Cuaca

Hallo 👋🏼 Nama saya Afrizal Mufriz Fouji dari kelas Flutter Jabar Coding Camp Batch 2.
Saya membuat Aplikasi untuk Project Challenge dari Jabar Digital Service yaitu Aplikasi Cuaca.
## Demo

Link YouTube : https://youtu.be/mxGBVv7q8Ck

Link APK : https://bit.ly/3jiKrCN

## Fitur

- Perkiraan Cuaca hari ini
- Perkiraan Cuaca 5 hari kedepan dengan data setiap 3 jam
- State Management GETX
