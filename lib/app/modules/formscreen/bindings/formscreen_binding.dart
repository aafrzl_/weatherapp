import 'package:get/get.dart';

import '../controllers/formscreen_controller.dart';

class FormscreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FormscreenController>(
      () => FormscreenController(),
    );
  }
}
