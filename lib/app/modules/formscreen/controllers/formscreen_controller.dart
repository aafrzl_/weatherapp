import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class FormscreenController extends GetxController {
  TextEditingController namaC = TextEditingController();
  RxBool hiddenKota = true.obs;
  RxBool hiddenProv = true.obs;
  RxBool hiddenButton = true.obs;
  RxBool connectionType = true.obs;

  final Connectivity _connectivity = Connectivity();

  late StreamSubscription _streamSubscription;

  List<String>? provinsi = [];

  List _kota = [];

  List<String>? kota = [];

  String? selectedKota;

  String selectedProvinsi = '';

  int provinsiIndex = -1;

  var parseData;

  @override
  void onInit() {
    super.onInit();

    _streamSubscription =
        _connectivity.onConnectivityChanged.listen(_updateState);
    GetConnectionType();
    parseJson();
  }

  @override
  void onReady() {
    super.onReady();
    _streamSubscription =
        _connectivity.onConnectivityChanged.listen(_updateState);
  }

  void showButton() {
    if (selectedKota != "" &&
        selectedProvinsi.isNotEmpty &&
        namaC.text.isNotEmpty) {
      hiddenButton.value = false;
    } else {
      hiddenButton.value = true;
    }
  }

  void setCities(index) {
    getCities(index);
  }

  getCities(index) {
    kota = [];
    _kota = [];
    _kota = parseData[index]['kota'];
    for (int i = 0; i < _kota.length; i++) {
      kota!.add(_kota[i]);
    }
  }

  void getProvinceIndex(String selectedProvince) {
    for (int i = 0; i < 34; i++) {
      if (provinsi![i] == selectedProvince) {
        provinsiIndex = i;
        break;
      }
    }
  }

  Future<void> parseJson() async {
    final String response =
        await rootBundle.loadString('assets/models/list_kota.json');
    parseData = await json.decode(response);

    for (int i = 0; i < 34; i++) {
      provinsi!.add(parseData[i]['provinsi']);
    }
  }

  Future<void> GetConnectionType() async {
    var connectivityResult;

    try {
      connectivityResult = await (_connectivity.checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
    }
    return _updateState(connectivityResult);
  }

  _updateState(ConnectivityResult result) {
    if (result == ConnectivityResult.wifi) {
      connectionType.value = true;
    } else if (result == ConnectivityResult.mobile) {
      connectionType.value = true;
    } else {
      connectionType.value = false;
    }
  }

  void refresh() {
    GetConnectionType();
  }
}
