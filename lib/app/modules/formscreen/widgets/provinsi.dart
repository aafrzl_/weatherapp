import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/formscreen_controller.dart';

class Provinsi extends GetView<FormscreenController> {
  const Provinsi({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: DropdownSearch<String>(
        label: "Provinsi",
        items: controller.provinsi,
        showSearchBox: true,
        showClearButton: true,
        searchBoxDecoration: InputDecoration(
          hintText: "Cari Provinsi",
          contentPadding: EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        onChanged: (prov) {
          if (prov != null) {
            controller.hiddenKota.value = false;
            controller.selectedProvinsi = prov;
            controller.getProvinceIndex(controller.selectedProvinsi);
            controller.getCities(controller.provinsiIndex);
          } else {
            controller.selectedProvinsi = "";
            controller.provinsiIndex = -1;
            controller.hiddenKota.value = true;
            controller.showButton();
          }
        },
        popupItemBuilder: (context, item, isSelected) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              "${item}",
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          );
        },
        itemAsString: (item) => item,
        selectedItem: controller.selectedProvinsi,
      ),
    );
  }
}
