import 'package:dropdown_search/dropdown_search.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:weatherapp/app/modules/formscreen/controllers/formscreen_controller.dart';

class Kota extends GetView<FormscreenController> {
  const Kota({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: DropdownSearch<String>(
        label: "Kota",
        items: controller.kota,
        showSearchBox: true,
        showClearButton: true,
        searchBoxDecoration: InputDecoration(
            hintText: "Cari Kota",
            contentPadding: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50),
            )),
        onChanged: (cityValue) {
          if (cityValue != null) {
            controller.selectedKota = cityValue;
          } else {
            controller.selectedKota = "";
          }
          controller.showButton();
        },
        popupItemBuilder: (context, item, isSelected) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(
              "${item}",
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          );
        },
        itemAsString: (item) => "${item}",
        selectedItem: controller.selectedKota,
      ),
    );
  }
}
