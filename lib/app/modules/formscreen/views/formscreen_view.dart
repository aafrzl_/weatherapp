import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:weatherapp/app/modules/formscreen/widgets/kota.dart';
import 'package:weatherapp/app/modules/formscreen/widgets/provinsi.dart';
import 'package:weatherapp/app/routes/app_pages.dart';

import '../controllers/formscreen_controller.dart';

class FormscreenView extends GetView<FormscreenController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.connectionType.value != false
          ? SafeArea(
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: Color(0xF8F8F8F8),
                body: Center(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/icons/09d.png",
                            height: 150,
                            width: 150,
                          ),
                          SizedBox(height: 10),
                          Text(
                            "Aplikasi Cuaca",
                            style: GoogleFonts.ptSans(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30, bottom: 20),
                            child: TextField(
                              autocorrect: false,
                              controller: controller.namaC,
                              decoration: InputDecoration(
                                labelText: "Nama",
                                hintText: "Masukkan nama",
                                border: OutlineInputBorder(),
                              ),
                              onChanged: (val) {
                                if (controller.namaC.text.isEmpty) {
                                  controller.hiddenProv.value = true;
                                  controller.hiddenKota.value = true;
                                  controller.hiddenButton.value = true;
                                } else {
                                  controller.hiddenProv.value = false;
                                }
                              },
                            ),
                          ),
                          controller.hiddenProv.isTrue
                              ? SizedBox()
                              : Provinsi(),
                          controller.hiddenKota.isTrue ? SizedBox() : Kota(),
                          SizedBox(height: 20),
                          controller.hiddenButton.isTrue
                              ? SizedBox()
                              : Container(
                                  width: double.infinity,
                                  child: ElevatedButton(
                                    onPressed: () =>
                                        Get.toNamed(Routes.HOME, arguments: [
                                      controller.namaC.text,
                                      controller.selectedKota.toString(),
                                    ]),
                                    child: Text(
                                      "Cek Cuaca",
                                      style: GoogleFonts.ptSans(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.yellowAccent,
                                      padding: EdgeInsets.symmetric(
                                        vertical: 20,
                                      ),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          : SafeArea(
              child: Scaffold(
                backgroundColor: Color(0xF8F8F8F8),
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/no-connection.png",
                        height: 200,
                        width: 200,
                      ),
                      Text(
                        "Tidak ada koneksi yang tersedia",
                        style: GoogleFonts.ptSans(
                          fontWeight: FontWeight.w300,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () => controller.refresh(),
                        child: Text("Refresh"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
