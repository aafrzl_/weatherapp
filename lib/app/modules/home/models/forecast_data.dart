import 'dart:convert';

Forecast forecastFromJson(String str) => Forecast.fromJson(json.decode(str));

// String forecastToJson(ListElement data) => json.encode(data.toJson());

class Forecast {
  String? cod;
  int? cnt;
  List<ListElement>? list;

  Forecast({
    this.cod,
    this.cnt,
    this.list,
  });

  factory Forecast.fromJson(Map<String, dynamic> json) => Forecast(
        cod: json["cod"],
        cnt: json["cnt"],
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
      );
}

class ListElement {
  int dt;
  MainClass main;
  List<WeatherForecast> weather;
  Clouds clouds;
  Wind wind;
  DateTime dtTxt;

  ListElement({
    required this.dt,
    required this.main,
    required this.weather,
    required this.clouds,
    required this.wind,
    required this.dtTxt,
  });

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        dt: json["dt"],
        main: MainClass.fromJson(json["main"]),
        weather: List<WeatherForecast>.from(
            json["weather"].map((x) => WeatherForecast.fromJson(x))),
        clouds: Clouds.fromJson(json["clouds"]),
        wind: Wind.fromJson(json["wind"]),
        dtTxt: DateTime.parse(json["dt_txt"]),
      );

  // Map<String, dynamic> toJson() => {
  //       "weather": List<dynamic>.from(weather.map((x) => x.toJson())),
  //     };
}

class Clouds {
  int? all;

  Clouds({
    this.all,
  });

  factory Clouds.fromJson(Map<String, dynamic> json) => Clouds(
        all: json["all"],
      );
}

class MainClass {
  double temp;
  int pressure;
  int humidity;

  MainClass({
    required this.temp,
    required this.pressure,
    required this.humidity,
  });

  factory MainClass.fromJson(Map<String, dynamic> json) => MainClass(
        temp: json["temp"].toDouble(),
        pressure: json["pressure"],
        humidity: json["humidity"],
      );
}

class WeatherForecast {
  String main;
  String description;
  String icon;

  WeatherForecast({
    required this.main,
    required this.description,
    required this.icon,
  });

  factory WeatherForecast.fromJson(Map<String, dynamic> json) =>
      WeatherForecast(
        main: json["main"],
        description: json["description"],
        icon: json["icon"],
      );
}

class Wind {
  double speed;

  Wind({
    required this.speed,
  });

  factory Wind.fromJson(Map<String, dynamic> json) => Wind(
        speed: json["speed"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "speed": speed,
      };
}
