import 'base_services.dart';
import 'package:dio/dio.dart';

class ForecastServices {
  BaseService service = BaseService();
  static const String apiKey = "f74e86a306c1df2164669e07e5bf4f43";
  static const String url = "https://api.openweathermap.org/data/2.5/forecast";

  Future<Response> getForecast(String cityName) async {
    try {
      Response response = await Dio()
          .get("$url?q=$cityName&appid=$apiKey&units=metric&lang=id");
      print("Response API Code : ${response.statusCode}");
      return response;
    } on DioError catch (e) {
      throw service.handleError(e);
    }
  }
}
