import 'package:dio/dio.dart';

import 'logger.dart';

class BaseService {
  final Dio _dio = Dio(BaseOptions(
      baseUrl: "https://samples.openweathermap.org",
      validateStatus: (status) {
        return status! < 500;
      },
      headers: {
        "Accept": "*/*",
        "Content-Type": "application/json",
        "Connection": "keep-alive",
      },
      connectTimeout: 60 * 1000,
      receiveTimeout: 60 * 1000))
    ..interceptors.add(LoggingInterceptor());

  handleError(DioError error) {
    print(error.response.toString());
    if (error.message.contains('SocketException')) {
      return 'Tidak bisa tersambung. Silahkan periksa koneksi internet.';
    }

    if (error.type == DioErrorType.connectTimeout) {
      return 'Koneksi bermasalah. Silahkan coba lagi.';
    }

    if (error.response == null || error.response!.data is String) {
      return 'Terjadi Kesalahan. Silahkan coba lagi';
    }
    return 'Terjadi Kesalahan. Silahkan coba lagi';
  }
}
