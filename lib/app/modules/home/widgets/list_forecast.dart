import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:weatherapp/app/modules/home/widgets/info_forecast.dart';

class CardForecast extends StatelessWidget {
  const CardForecast({
    Key? key,
    required this.icon,
    required this.dtTxt,
    required this.description,
    required this.temp,
    required this.infoHumadity,
    required this.infoPressure,
    required this.infoCloudness,
    required this.infoWind,
  }) : super(key: key);

  final String icon;
  final DateTime dtTxt;
  final String description;
  final String temp;
  final String infoHumadity;
  final String infoPressure;
  final String infoCloudness;
  final String infoWind;

  @override
  Widget build(BuildContext context) {
    return ExpandablePanel(
      header: Row(
        children: [
          getWeatherIcon(icon),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
                  child: Text(
                    DateFormat('EEEE, dd/MM/yy kk:mm a', 'id_ID')
                        .format(dtTxt)
                        .toString(),
                    style: GoogleFonts.ptSans(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Text(
                  description,
                  style: GoogleFonts.ptSans(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "${temp} °C",
                style: GoogleFonts.ptSans(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ],
      ),
      collapsed: SizedBox(),
      expanded: Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InformasiForecast(
                  path: "assets/images/humidity.svg",
                  info: "${infoHumadity}%",
                  title: "Humidity",
                ),
                InformasiForecast(
                  path: "assets/images/pressure.svg",
                  info: "${infoPressure} hpa",
                  title: "Pressure",
                ),
                InformasiForecast(
                  path: "assets/images/cloud.svg",
                  info: "${infoCloudness}%",
                  title: "Cloudness",
                ),
                InformasiForecast(
                  path: "assets/images/wind.svg",
                  info: "${infoWind} m/s",
                  title: "Wind",
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Image getWeatherIcon(String _icon) {
  String path = 'assets/icons/';
  String imageExtension = ".png";
  return Image.asset(
    path + _icon + imageExtension,
    width: 50,
    height: 50,
  );
}
