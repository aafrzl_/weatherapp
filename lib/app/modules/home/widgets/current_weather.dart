import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CurrentWeather extends StatelessWidget {
  const CurrentWeather({
    Key? key,
    required this.greeting,
    required this.temp,
    required this.icon,
    required this.weather,
  }) : super(key: key);

  final String greeting;
  final String temp;
  final String icon;
  final String weather;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Selamat $greeting",
                style: GoogleFonts.pacifico(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
              Text(
                "$temp°C",
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  fontSize: 35,
                  fontWeight: FontWeight.normal,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          Column(
            children: [
              getWeatherIcon(icon),
              Text(
                weather,
                textAlign: TextAlign.center,
                style: GoogleFonts.ptSans(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

Image getWeatherIcon(String _icon) {
  String path = 'assets/icons/';
  String imageExtension = ".png";
  return Image.asset(
    path + _icon + imageExtension,
    width: 80,
    height: 80,
  );
}
