import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class InformasiForecast extends StatelessWidget {
  const InformasiForecast({
    Key? key,
    required this.path,
    required this.info,
    required this.title,
  }) : super(key: key);

  final String path;
  final String info;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.asset(
          path,
          color: Colors.white,
          height: 30,
          width: 30,
        ),
        SizedBox(height: 5),
        Text(
          info,
          style: GoogleFonts.ptSans(
            fontWeight: FontWeight.normal,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}
