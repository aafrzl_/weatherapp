import 'package:flutter/material.dart';
import 'package:weatherapp/app/modules/home/widgets/info.dart';

class InformasiTambahan extends StatelessWidget {
  const InformasiTambahan({
    Key? key,
    required this.infoHumidity,
    required this.infoPressure,
    required this.infoCloudness,
    required this.infoWind,
  }) : super(key: key);

  final String infoHumidity;
  final String infoPressure;
  final String infoCloudness;
  final String infoWind;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.2),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Informasi(
                  path: "assets/images/humidity.svg",
                  info: "$infoHumidity%",
                  title: "Humidity",
                ),
                Informasi(
                  path: "assets/images/pressure.svg",
                  info: "$infoPressure hpa",
                  title: "Pressure",
                ),
                Informasi(
                  path: "assets/images/cloud.svg",
                  info: "$infoCloudness%",
                  title: "Cloudness",
                ),
                Informasi(
                  path: "assets/images/wind.svg",
                  info: "$infoWind m/s",
                  title: "Wind",
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
