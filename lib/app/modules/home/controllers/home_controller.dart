import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:weatherapp/app/modules/home/services/forecast_services.dart';
import 'package:weatherapp/app/modules/home/services/weather_service.dart';
import 'package:weatherapp/app/utilities/snack_bar.dart';

import '../models/current_weather_data.dart';
import '../models/forecast_data.dart';

class HomeController extends GetxController {
  var timeNow = int.parse(DateFormat('kk').format(DateTime.now()));
  final dynamic argumenData = Get.arguments;
  final weatherService = Get.put(WeatherService());
  final forecastService = Get.put(ForecastServices());

  RxBool connectionType = true.obs;

  final Connectivity _connectivity = Connectivity();

  late StreamSubscription _streamSubscription;

  var message = '';
  var imageName = '';

  void refresh() {
    getWeatherData();
    timeGreeting();
    getConnectionType();
  }

  void timeGreeting() {
    if (timeNow >= 6 && timeNow < 12) {
      message = 'Pagi';
      imageName = 'Morning';
    } else if (timeNow >= 12 && timeNow < 15) {
      message = 'Siang';
      imageName = 'Afternoon';
    } else if (timeNow >= 15 && timeNow < 18) {
      message = 'Sore';
      imageName = 'Evening';
    } else {
      message = 'Malam';
      imageName = 'Night';
    }
  }

  Future<Weather> getWeatherData() async {
    var res;
    try {
      res = await weatherService.getWeather(argumenData[1].toString());
      if (res.statusCode != 200) {
        return WeatherSnackBars.errorSnackBar(message: res.data['message']);
      }
    } on DioError catch (e) {
      print(e.toString());
      return WeatherSnackBars.errorSnackBar(message: e.toString());
    }
    return Weather.fromJson(res.data);
  }

  Future<Forecast> getForecastData() async {
    var res;
    try {
      res = await forecastService.getForecast(argumenData[1].toString());
      if (res.statusCode != 200) {
        return WeatherSnackBars.errorSnackBar(message: res.data['message']);
      }
    } on DioError catch (e) {
      print(e.toString());
      return WeatherSnackBars.errorSnackBar(message: e.toString());
    }
    return Forecast.fromJson(res.data);
  }

  Future<void> getConnectionType() async {
    var connectivityResult;

    try {
      connectivityResult = await (_connectivity.checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
    }
    return _updateState(connectivityResult);
  }

  _updateState(ConnectivityResult result) {
    if (result == ConnectivityResult.wifi) {
      connectionType.value = true;
    } else if (result == ConnectivityResult.mobile) {
      connectionType.value = true;
    } else {
      connectionType.value = false;
    }
  }

  @override
  void onInit() {
    super.onInit();
    print(argumenData[1].toString());
    _streamSubscription =
        _connectivity.onConnectivityChanged.listen(_updateState);
    timeGreeting();
  }

  @override
  void onReady() {
    super.onReady();
    _streamSubscription =
        _connectivity.onConnectivityChanged.listen(_updateState);
    timeGreeting();
  }

  @override
  void onClose() {}
}
