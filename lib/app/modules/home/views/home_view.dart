import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:weatherapp/app/modules/home/models/forecast_data.dart';
import 'package:weatherapp/app/modules/home/widgets/current_weather.dart';
import 'package:weatherapp/app/modules/home/widgets/list_forecast.dart';

import '../controllers/home_controller.dart';
import '../models/current_weather_data.dart';
import '../widgets/info_tambahan.dart';

class HomeView extends GetView<HomeController> {
  final dynamic argumenData = Get.arguments;

  @override
  Widget build(BuildContext context) {
    String message = controller.message;

    String name = argumenData[0].toString();
    String kota = argumenData[1].toString();

    return Obx(
      () => controller.connectionType.value != false
          ? FutureBuilder<Weather>(
              future: controller.getWeatherData(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return SafeArea(
                    child: Scaffold(
                      backgroundColor: Color(0xF8F8F8F8),
                      body: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/bandage.png",
                              height: 200,
                              width: 200,
                            ),
                            Text(
                              "Maaf kota yang anda pilih tidak tersedia!",
                              style: GoogleFonts.ptSans(
                                fontWeight: FontWeight.w300,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 10),
                            ElevatedButton(
                              onPressed: () => Get.back(),
                              child: Text("Kembali"),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                } else if (snapshot.hasData) {
                  var data = snapshot.data;

                  return SafeArea(
                    child: Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl:
                              "https://source.unsplash.com/random/?${data!.weather[0].main},${controller.imageName}",
                          fit: BoxFit.cover,
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black.withOpacity(0.5),
                          colorBlendMode: BlendMode.multiply,
                        ),
                        Scaffold(
                          backgroundColor: Colors.transparent,
                          appBar: AppBar(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.transparent,
                            elevation: 0,
                            centerTitle: true,
                            title: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(width: 5),
                                    Text(
                                      kota,
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.ptSans(
                                        fontWeight: FontWeight.w300,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  DateFormat("EEEE, d MMMM yyyy", "id_ID")
                                      .format(DateTime.now())
                                      .toString(),
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.ptSans(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                            actions: [
                              IconButton(
                                onPressed: () => controller.refresh(),
                                icon: Icon(Icons.refresh),
                              ),
                            ],
                          ),
                          body: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CurrentWeather(
                                  greeting: "$message $name",
                                  icon: "${data.weather[0].icon}",
                                  temp: "${data.main.temp.floor()}",
                                  weather:
                                      "${data.weather[0].description.capitalizeFirst}",
                                ),
                                SizedBox(height: 30),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                    bottom: 8,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Informasi Tambahan",
                                        style: GoogleFonts.ptSans(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                InformasiTambahan(
                                  infoHumidity: "${data.main.humidity}",
                                  infoPressure: "${data.main.pressure}",
                                  infoCloudness: "${data.clouds.all}",
                                  infoWind: "${data.wind.speed}",
                                ),
                                SizedBox(height: 10),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 10,
                                    bottom: 8,
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Perkiraan cuaca",
                                        textAlign: TextAlign.left,
                                        style: GoogleFonts.ptSans(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                FutureBuilder<Forecast>(
                                  future: controller.getForecastData(),
                                  builder: ((context, snapshot) {
                                    if (snapshot.hasError) {
                                      return Center(
                                        child: Text(
                                            "Data forecast tidak tersedia"),
                                      );
                                    } else if (snapshot.hasData) {
                                      var data = snapshot.data;

                                      return ExpandableTheme(
                                        data: const ExpandableThemeData(
                                          iconColor: Colors.white,
                                          useInkWell: true,
                                        ),
                                        child: Expanded(
                                          child: ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: data!.list!.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: CardForecast(
                                                  icon:
                                                      "${data.list![index].weather[0].icon}",
                                                  dtTxt:
                                                      data.list![index].dtTxt,
                                                  description:
                                                      "${data.list![index].weather[0].description.toString().capitalize}",
                                                  temp:
                                                      "${data.list![index].main.temp.floor()}",
                                                  infoHumadity:
                                                      "${data.list![index].main.humidity}",
                                                  infoPressure:
                                                      "${data.list![index].main.pressure}",
                                                  infoCloudness:
                                                      "${data.list![index].clouds.all}",
                                                  infoWind:
                                                      "${data.list![index].wind.speed}",
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      );
                                    }
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            )
          : SafeArea(
              child: Scaffold(
                backgroundColor: Color(0xF8F8F8F8),
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/no-connection.png",
                        height: 200,
                        width: 200,
                      ),
                      Text(
                        "Tidak ada koneksi yang tersedia",
                        style: GoogleFonts.ptSans(
                          fontWeight: FontWeight.w300,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: () => controller.refresh(),
                        child: Text("Refresh"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
