import 'package:get/get.dart';

import 'package:weatherapp/app/modules/formscreen/bindings/formscreen_binding.dart';
import 'package:weatherapp/app/modules/formscreen/views/formscreen_view.dart';
import 'package:weatherapp/app/modules/home/bindings/home_binding.dart';
import 'package:weatherapp/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.FORMSCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.FORMSCREEN,
      page: () => FormscreenView(),
      binding: FormscreenBinding(),
    ),
  ];
}
